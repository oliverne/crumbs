import { render } from "react-dom";
import { App } from "./App.js";

// JSX
render(<App />, document.getElementById("root"));
